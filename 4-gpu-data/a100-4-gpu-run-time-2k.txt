wangpeng@rongtian-SYS-7049GP-TRT:~/multi-gpu-evert-cuda$ ./4-evert-cuda
gpus_init 38.260411

img_w     960
img_h     540
nframes   7
nsamples  16
nbounces  16

img dir                   .
triangles nelems          57,600
triangles nbytes          2,534,400

nintersections any frame  7,644,119,040,000
nintersections all frames 53,508,833,280,000

0000  0.000  mesh_shdr 0.000513  ^C
wangpeng@rongtian-SYS-7049GP-TRT:~/multi-gpu-evert-cuda$ ./4-evert-cuda
gpus_init 2.338649

img_w     960
img_h     540
nframes   7
nsamples  16
nbounces  16

img dir                   .
triangles nelems          57,600
triangles nbytes          2,534,400

nintersections any frame  7,644,119,040,000
nintersections all frames 53,508,833,280,000

0000  0.000  mesh_shdr 0.000307  px_shdr 26.563  px/s 19,516  prim/s 2,168  rays/s 4,996,023  ints/s 287,770,913,514  ppm 0.002  
0001  2.000  mesh_shdr 0.000635  px_shdr 26.880  px/s 19,285  prim/s 2,143  rays/s 4,937,073  ints/s 284,375,401,043  ppm 0.002  
0002  4.000  mesh_shdr 0.000828  px_shdr 26.714  px/s 19,405  prim/s 2,156  rays/s 4,967,748  ints/s 286,142,274,905  ppm 0.002  
0003  6.000  mesh_shdr 0.000831  px_shdr 26.576  px/s 19,506  prim/s 2,167  rays/s 4,993,543  ints/s 287,628,094,592  ppm 0.002  
0004  8.000  mesh_shdr 0.000828  px_shdr 26.769  px/s 19,366  prim/s 2,152  rays/s 4,957,613  ints/s 285,558,485,578  ppm 0.002  
0005 10.000  mesh_shdr 0.000641  px_shdr 26.860  px/s 19,300  prim/s 2,144  rays/s 4,940,766  ints/s 284,588,095,954  ppm 0.002  
0006 12.000  mesh_shdr 0.000559  px_shdr 26.554  px/s 19,522  prim/s 2,169  rays/s 4,997,751  ints/s 287,870,479,714  ppm 0.002  

gpus_free 0.109313
took 187.923 seconds.
