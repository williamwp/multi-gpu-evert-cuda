Exporting 3847 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-e8d4-b561-eb3b-085e.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   88.4      2683922236           3     894640745.3       828473308       982608904  cudaEventSynchronize                                                            
    9.8       298747631           9      33194181.2            2098       297345708  cudaMallocArray                                                                 
    1.0        29493021           9       3277002.3           12113        10229383  cudaMemcpyToArray                                                               
    0.7        20253410           3       6751136.7         5918450         7189920  cudaMemcpy                                                                      
    0.0          883801           3        294600.3          248953          356218  cudaFree                                                                        
    0.0          641014           3        213671.3          146089          264135  cudaMalloc                                                                      
    0.0          387580           3        129193.3          127621          132150  cudaFreeArray                                                                   
    0.0          203733           3         67911.0           48574           79639  cudaLaunchKernel                                                                
    0.0           46411           6          7735.2            2991           22551  cudaEventRecord                                                                 
    0.0           33925           9          3769.4             874            9084  cudaUnbindTexture                                                               
    0.0           33710           9          3745.6            1247            8153  cudaBindTextureToArray                                                          
    0.0           16347           6          2724.5             694            7045  cudaEventCreate                                                                 
    0.0           10836           6          1806.0             387            4407  cudaEventDestroy                                                                




Generating CUDA Kernel Statistics...
CUDA Kernel Statistics (nanoseconds)

Time(%)      Total Time   Instances         Average         Minimum         Maximum  Name                                                                                                                                                                                                                                                                                                                                         
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------------------------------------------                                                                                                                                                                                                                         
  100.0      2683862016           3     894620672.0       828454426       982587404  kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)                                                                                                                                                                                        



Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   62.6        28724421           9       3191602.3            2880         9993828  [CUDA memcpy HtoA]                                                              
   37.4        17181822           3       5727274.0         4889157         6174604  [CUDA memcpy DtoH]                                                              


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
          32400.000               3            10800.000          10800.000            10800.000  [CUDA memcpy DtoH]                                                              
         129606.000               9            14400.667              1.000            43200.000  [CUDA memcpy HtoA]                                                              




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   96.8      3807165263          49      77697250.3           24815       100214316  poll                                                                            
    2.7       105444536         975        108148.2            1008        18723345  ioctl                                                                           
    0.2         6516581          38        171489.0            1209         5730805  fopen                                                                           
    0.1         2793784           9        310420.4            1085         2778900  fclose                                                                          
    0.1         2722741         526          5176.3            1460           22648  fwrite                                                                          
    0.1         2537954          78         32537.9            1146          767751  mmap                                                                            
    0.0         1860434         505          3684.0            1469           14120  fread                                                                           
    0.0         1682888          89         18908.9            3685           25221  open64                                                                          
    0.0         1287931          10        128793.1           15993          689662  sem_timedwait                                                                   
    0.0          168264           4         42066.0           35640           54480  pthread_create                                                                  
    0.0           97189           3         32396.3           26516           40642  fgets                                                                           
    0.0           59609           6          9934.8            6388           23741  putc                                                                            
    0.0           39484          11          3589.5            2120            6109  write                                                                           
    0.0           37850           5          7570.0            2746           10937  open                                                                            
    0.0           29040          13          2233.8            1009            3353  read                                                                            
    0.0           20264           6          3377.3            1106            4928  munmap                                                                          
    0.0           12436           2          6218.0            4828            7608  socket                                                                          
    0.0            7203           1          7203.0            7203            7203  connect                                                                         
    0.0            6133           1          6133.0            6133            6133  pipe2                                                                           
    0.0            4629           2          2314.5            2062            2567  fcntl                                                                           
    0.0            1134           1          1134.0            1134            1134  bind                                                                            




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/4-Film-grain-rendering-gpu-sample1/bin/report3.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/4-Film-grain-rendering-gpu-sample1/bin/report3.sqlite"

