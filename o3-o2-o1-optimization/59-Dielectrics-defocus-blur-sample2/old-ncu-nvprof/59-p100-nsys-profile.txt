
Importing [==================================================100%]
Saving report to file "/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report3.qdrep"
Report file saved.
Please discard the qdstrm file and use the qdrep file instead.

Removed /home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report3.qdstrm as it was successfully imported.
Please use the qdrep file instead.

Exporting the qdrep file to SQLite database using /usr/local/cuda-10.2/nsight-systems-2019.5.2/host-linux-x64/nsys-exporter.

Exporting 21023 events:

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report3.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.9    767487767087        7691      99790374.1            1163       100214731  poll                                                                            
    0.1       757554813       10757         70424.4            1021        35694258  fwrite                                                                          
    0.0       251097466        1514        165850.4            1002        76314906  ioctl                                                                           
    0.0        13551720          92        147301.3            1984         8033932  mmap                                                                            
    0.0         1408481          30         46949.4            1774         1272351  fopen                                                                           
    0.0         1204181          59         20409.8            6058           30358  open64                                                                          
    0.0          833188          18         46288.2           14690           83326  sem_timedwait                                                                   
    0.0          509810          33         15448.8            2751          279319  munmap                                                                          
    0.0          308088           7         44012.6            4139          228812  fread                                                                           
    0.0          248271           3         82757.0           78435           90200  fgets                                                                           
    0.0          122562           1        122562.0          122562          122562  pthread_join                                                                    
    0.0          101176          20          5058.8            2597            7988  write                                                                           
    0.0           85162           2         42581.0           41423           43739  pthread_create                                                                  
    0.0           55861          22          2539.1            1217            4154  read                                                                            
    0.0           48311          24          2013.0            1335            3728  fclose                                                                          
    0.0           29641           4          7410.2            4462           12438  open                                                                            
    0.0           12602           8          1575.3            1000            3982  fcntl                                                                           
    0.0           12242           2          6121.0            5393            6849  socket                                                                          
    0.0            9127           1          9127.0            9127            9127  connect                                                                         
    0.0            7644           1          7644.0            7644            7644  pipe2                                                                           
    0.0            2187           1          2187.0            2187            2187  bind                                                                            
    0.0            1054           1          1054.0            1054            1054  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)

