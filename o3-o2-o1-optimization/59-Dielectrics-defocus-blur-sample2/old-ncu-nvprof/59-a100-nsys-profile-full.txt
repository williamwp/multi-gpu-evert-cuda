Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-bf63-dd16-9c91-e777.qdrep"
Exporting 28023 events: [=================================================100%]

Exported successfully to
/tmp/nsys-report-bf63-dd16-9c91-e777.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.1     41555907730           5    8311181546.0           11386     38458570924  cudaDeviceSynchronize                                                           
    0.6       237667364           1     237667364.0       237667364       237667364  cudaMallocManaged                                                               
    0.2        88621086           1      88621086.0        88621086        88621086  cudaDeviceReset                                                                 
    0.1        42320948           5       8464189.6            6343        37507217  cudaFree                                                                        
    0.0         3074521           5        614904.2           56149         2545148  cudaLaunchKernel                                                                
    0.0         1540337           5        308067.4            7732         1243276  cudaMalloc                                                                      
    0.0            8005           1          8005.0            8005            8005  cuCtxSynchronize                                                                




Generating CUDA Kernel Statistics...
CUDA Kernel Statistics (nanoseconds)

Time(%)      Total Time   Instances         Average         Minimum         Maximum  Name                                                                                                                                                                                                                                                                                                                                         
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------------------------------------------                                                                                                                                                                                                                         
   92.5     38458544576           1   38458544576.0     38458544576     38458544576  render(vec3*, int, int, int, camera**, hitable**, curandStateXORWOW*)                                                                                                                                                                                                                                                                        
    7.3      3056525474           1    3056525474.0      3056525474      3056525474  render_init(int, int, curandStateXORWOW*)                                                                                                                                                                                                                                                                                                    
    0.1        40703172           1      40703172.0        40703172        40703172  create_world(hitable**, hitable**, camera**, int, int, curandStateXORWOW*)                                                                                                                                                                                                                                                                   
    0.1        37451142           1      37451142.0        37451142        37451142  free_world(hitable**, hitable**, camera**)                                                                                                                                                                                                                                                                                                   
    0.0            4097           1          4097.0            4097            4097  rand_init(curandStateXORWOW*)                                                                                                                                                                                                                                                                                                                



Generating CUDA Memory Operation Statistics...
CUDA Memory Operation Statistics (nanoseconds)

Time(%)      Total Time  Operations         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
  100.0         2116525         367          5767.1            1400           39192  [CUDA Unified Memory memcpy DtoH]                                               


CUDA Memory Operation Statistics (KiB)

              Total      Operations              Average            Minimum              Maximum  Name                                                                            
-------------------  --------------  -------------------  -----------------  -------------------  --------------------------------------------------------------------------------
          43200.000             367              117.711              4.000             1020.000  [CUDA Unified Memory memcpy DtoH]                                               




Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   90.2     83493848768        4209      19836980.0            1000       100237012  poll                                                                            
    8.5      7856579965        3743       2099006.1           12592        21247578  sem_timedwait                                                                   
    1.1      1060266222       16426         64548.0            1000        55035268  fwrite                                                                          
    0.2       157213240        1327        118472.7            1015        24982131  ioctl                                                                           
    0.0        13347252         131        101887.4            1133         4210408  mmap                                                                            
    0.0         5559700          36        154436.1            1328         5437888  fopen                                                                           
    0.0         1846306          89         20745.0            5005           34257  open64                                                                          
    0.0          628055          37         16974.5            1593          358814  munmap                                                                          
    0.0          198034           4         49508.5           45217           55312  pthread_create                                                                  
    0.0          157158           3         52386.0           50634           54818  fgets                                                                           
    0.0          152786           1        152786.0          152786          152786  pthread_join                                                                    
    0.0          132961           6         22160.2            1995           76580  fread                                                                           
    0.0           89523          22          4069.2            1308            6067  write                                                                           
    0.0           49516          21          2357.9            1041            3050  read                                                                            
    0.0           34538           5          6907.6            3556           11019  open                                                                            
    0.0           30807          20          1540.3            1001            3185  fclose                                                                          
    0.0            9007           2          4503.5            4421            4586  socket                                                                          
    0.0            7442           1          7442.0            7442            7442  connect                                                                         
    0.0            6349           1          6349.0            6349            6349  pipe2                                                                           
    0.0            3985           2          1992.5            1088            2897  fcntl                                                                           
    0.0            2154           1          2154.0            2154            2154  bind                                                                            
    0.0            1692           1          1692.0            1692            1692  pthread_mutex_trylock                                                           
    0.0            1576           1          1576.0            1576            1576  fflush                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)




Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report2.qdrep"
Report file moved to "/home/wangpeng/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report2.sqlite"

