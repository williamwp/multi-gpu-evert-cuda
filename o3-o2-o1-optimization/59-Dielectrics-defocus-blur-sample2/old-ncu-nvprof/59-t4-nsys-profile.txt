test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2$ /usr/local/cuda-11.4/bin/nsys  profile --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Rendering a 2560x1440 image with 100 samples per pixel in 32x32 blocks.
CUDA error = 701 at Dielectrics-defocus-blur.cu:194 'cudaGetLastError()' 

The target application returned non-zero exit code 99
Processing events...
Saving temporary "/tmp/nsys-report-087f-a303-9039-d065.qdstrm" file to disk...

Creating final output files...
Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-087f-a303-9039-d065.qdrep"
Exporting 4851 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-087f-a303-9039-d065.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls      Average        Minimum       Maximum          StdDev               Name         
 -------  ---------------  ---------  ---------------  -----------  --------------  ---------------  ---------------------
    99.1   16,909,084,048          3  5,636,361,349.3       10,905  16,867,004,779  9,726,045,244.1  cudaDeviceSynchronize
     0.7      115,534,364          1    115,534,364.0  115,534,364     115,534,364              0.0  cudaMallocManaged    
     0.2       41,415,250          1     41,415,250.0   41,415,250      41,415,250              0.0  cudaDeviceReset      
     0.0        2,520,878          4        630,219.5       11,453       2,252,288      1,084,420.2  cudaLaunchKernel     
     0.0          955,173          5        191,034.6        6,659         759,804        325,366.6  cudaMalloc           
     0.0            2,397          1          2,397.0        2,397           2,397              0.0  cuCtxSynchronize     



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances      Average          Minimum         Maximum      StdDev                                     Name                                   
 -------  ---------------  ---------  ----------------  --------------  --------------  ------  --------------------------------------------------------------------------
    99.8   16,866,992,847          1  16,866,992,847.0  16,866,992,847  16,866,992,847     0.0  render_init(int, int, curandStateXORWOW*)                                 
     0.2       42,070,724          1      42,070,724.0      42,070,724      42,070,724     0.0  create_world(hitable**, hitable**, camera**, int, int, curandStateXORWOW*)
     0.0            4,448          1           4,448.0           4,448           4,448     0.0  rand_init(curandStateXORWOW*)                                             



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum       StdDev          Name     
 -------  ---------------  ---------  ------------  -------  -----------  ------------  --------------
    91.1   31,170,558,316      1,590  19,604,124.7    5,634  100,170,530  27,836,588.4  poll          
     8.6    2,950,614,196      1,418   2,080,828.1    5,221   21,235,828     720,573.6  sem_timedwait 
     0.2       71,345,548      1,046      68,208.0    1,001    8,911,751     399,894.4  ioctl         
     0.0        3,192,779        115      27,763.3    1,669      762,731      81,242.5  mmap64        
     0.0        1,264,433         26      48,632.0    1,044      234,353      75,000.4  mmap          
     0.0          596,827         27      22,104.7    1,057      536,345     102,789.2  fopen         
     0.0          466,575         88       5,302.0    2,033       11,033       1,378.0  open64        
     0.0          283,720         35       8,106.3    1,551      127,615      21,432.4  munmap        
     0.0           65,162          1      65,162.0   65,162       65,162           0.0  pthread_join  
     0.0           62,728          6      10,454.7    1,337       33,594      13,034.6  fread         
     0.0           60,370         21       2,874.8    1,001       15,716       3,054.0  write         
     0.0           52,979          4      13,244.8   11,225       15,795       1,898.5  pthread_create
     0.0           24,152          1      24,152.0   24,152       24,152           0.0  fgets         
     0.0           14,988          6       2,498.0    1,501        4,076         936.4  open          
     0.0            8,444          2       4,222.0    3,224        5,220       1,411.4  fwrite        
     0.0            6,374          4       1,593.5    1,024        2,437         635.7  fclose        
     0.0            5,992          2       2,996.0    2,200        3,792       1,125.7  fgetc         
     0.0            4,128          1       4,128.0    4,128        4,128           0.0  connect       
     0.0            3,745          2       1,872.5    1,369        2,376         712.1  socket        
     0.0            2,757          1       2,757.0    2,757        2,757           0.0  pipe2         
     0.0            1,604          1       1,604.0    1,604        1,604           0.0  fcntl         
     0.0            1,233          1       1,233.0    1,233        1,233           0.0  read          

Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report4.qdrep"
Report file moved to "/home/test/tools/backup6/GPU-rendering-programs/59-Dielectrics-defocus-blur-sample2/report4.sqlite"

