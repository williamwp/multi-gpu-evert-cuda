    Kernel: render_kernel(int, int, int, Polygon*, float3*, float3*, float3, float3)
         21                             inst_per_warp                                                 Instructions per warp  1.2047e+05  1.2054e+05  1.2047e+05
         21                         branch_efficiency                                                     Branch Efficiency      87.90%      87.90%      87.90%
         21                 warp_execution_efficiency                                             Warp Execution Efficiency      72.85%      72.90%      72.89%
         21         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      69.99%      70.03%      70.03%
         21                      inst_replay_overhead                                           Instruction Replay Overhead    0.000026    0.000027    0.000026
         21      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         21     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         21       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         21      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         21              gld_transactions_per_request                                  Global Load Transactions Per Request   15.088964   15.088964   15.088964
         21              gst_transactions_per_request                                 Global Store Transactions Per Request   16.000000   16.000000   16.000000
         21                 shared_store_transactions                                             Shared Store Transactions           0           0           0
         21                  shared_load_transactions                                              Shared Load Transactions           0           0           0
         21                   local_load_transactions                                               Local Load Transactions           0           0           0
         21                  local_store_transactions                                              Local Store Transactions           0           0           0
         21                          gld_transactions                                              Global Load Transactions  1.2518e+10  1.2518e+10  1.2518e+10
         21                          gst_transactions                                             Global Store Transactions     6220800     6220800     6220800
         21                  sysmem_read_transactions                                       System Memory Read Transactions         744         913         891
         21                 sysmem_write_transactions                                      System Memory Write Transactions           5          35           7
         21                      l2_read_transactions                                                  L2 Read Transactions     3447588     4158165     3533201
         21                     l2_write_transactions                                                 L2 Write Transactions     6901950     6940999     6934652
         21                    dram_read_transactions                                       Device Memory Read Transactions      781737     1220896      811659
         21                   dram_write_transactions                                      Device Memory Write Transactions     2079827     2167248     2156887
         21                           global_hit_rate                                     Global Hit Rate in unified l1/tex      99.72%      99.72%      99.72%
         21                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
         21                  gld_requested_throughput                                      Requested Global Load Throughput  52.276GB/s  52.327GB/s  52.318GB/s
         21                  gst_requested_throughput                                     Requested Global Store Throughput  740.28MB/s  741.04MB/s  740.92MB/s
         21                            gld_throughput                                                Global Load Throughput  437.15GB/s  437.58GB/s  437.51GB/s
         21                            gst_throughput                                               Global Store Throughput  2.8917GB/s  2.8947GB/s  2.8942GB/s
         21                     local_memory_overhead                                                 Local Memory Overhead       0.00%       0.00%       0.00%
         21                        tex_cache_hit_rate                                                Unified Cache Hit Rate      99.75%      99.75%      99.75%
         21                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      70.08%      70.21%      70.14%
         21                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      91.67%      91.67%      91.67%
         21                      dram_read_throughput                                         Device Memory Read Throughput  372.44MB/s  581.66MB/s  386.69MB/s
         21                     dram_write_throughput                                        Device Memory Write Throughput  990.86MB/s  1.0084GB/s  1.0035GB/s
         21                      tex_cache_throughput                                              Unified Cache Throughput  1542.6GB/s  1544.2GB/s  1543.9GB/s
         21                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  1.2110GB/s  1.2126GB/s  1.2118GB/s
         21                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  2.8917GB/s  2.8947GB/s  2.8942GB/s
         21                        l2_read_throughput                                                 L2 Throughput (Reads)  1.6040GB/s  1.9347GB/s  1.6438GB/s
         21                       l2_write_throughput                                                L2 Throughput (Writes)  3.2112GB/s  3.2294GB/s  3.2263GB/s
         21                    sysmem_read_throughput                                         System Memory Read Throughput  362.96KB/s  445.42KB/s  435.02KB/s
         21                   sysmem_write_throughput                                        System Memory Write Throughput  2.4365KB/s  17.075KB/s  3.4141KB/s
         21                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         21                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         21                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         21                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         21                            gld_efficiency                                         Global Memory Load Efficiency      11.96%      11.96%      11.96%
         21                            gst_efficiency                                        Global Memory Store Efficiency      25.00%      25.00%      25.00%
         21                    tex_cache_transactions                                            Unified Cache Transactions  3318537600  3318537600  3318537600
         21                             flop_count_dp                           Floating Point Operations(Double Precision)   186624000   186624000   186624000
         21                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)    37324800    37324800    37324800
         21                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)    74649600    74649600    74649600
         21                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)           0           0           0
         21                             flop_count_sp                           Floating Point Operations(Single Precision)  1.1669e+11  1.1671e+11  1.1670e+11
         21                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)  2.4972e+10  2.4976e+10  2.4974e+10
         21                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  3.8597e+10  3.8602e+10  3.8600e+10
         21                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)  1.4525e+10  1.4527e+10  1.4526e+10
         21                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)  2348202252  2348667836  2348618616
         21                             inst_executed                                                 Instructions Executed  7806140475  7811168181  7806581325
         21                               inst_issued                                                   Instructions Issued  7806347466  7811373307  7806787229
         21                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
         21                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
         21                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)      14.90%      14.92%      14.91%
         21                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      13.32%      13.33%      13.32%
         21                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       4.30%       4.32%       4.32%
         21                             stall_texture                                         Issue Stall Reasons (Texture)       0.76%       0.76%       0.76%
         21                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
         21                               stall_other                                           Issue Stall Reasons (Other)      60.44%      60.45%      60.44%
         21          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.01%       0.01%       0.01%
         21                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       0.51%       0.51%       0.51%
         21                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
         21                                inst_fp_32                                               FP Instructions(Single)  9.1907e+10  9.1918e+10  9.1912e+10
         21                                inst_fp_64                                               FP Instructions(Double)   149299200   149299200   149299200
         21                              inst_integer                                                  Integer Instructions  2.7625e+10  2.7630e+10  2.7625e+10
         21                          inst_bit_convert                                              Bit-Convert Instructions   348364800   348364800   348364800
         21                              inst_control                                             Control-Flow Instructions  1.2252e+10  1.2255e+10  1.2252e+10
         21                        inst_compute_ld_st                                               Load/Store Instructions  2.6561e+10  2.6561e+10  2.6561e+10
         21                                 inst_misc                                                     Misc Instructions  1.6084e+10  1.6087e+10  1.6085e+10
         21           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
         21                               issue_slots                                                           Issue Slots  7149459368  7153799658  7149848527
         21                                 cf_issued                                      Issued Control-Flow Instructions   882465647   883995282   882573791
         21                               cf_executed                                    Executed Control-Flow Instructions   882465647   883995282   882573791
         21                               ldst_issued                                        Issued Load/Store Instructions  3320352000  3320352000  3320352000
         21                             ldst_executed                                      Executed Load/Store Instructions   830282400   830282400   830282400
         21                       atomic_transactions                                                   Atomic Transactions           0           0           0
         21           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
         21                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
         21                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
         21                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)     2603578     2606189     2604634
         21                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
         21                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       5.73%       5.73%       5.73%
         21                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)     6220800     6220800     6220800
         21                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
         21                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
         21                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
         21                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
         21                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
         21                                       ipc                                                          Executed IPC    1.673729    1.674662    1.674358
         21                                issued_ipc                                                            Issued IPC    1.673844    1.674827    1.674397
         21                    issue_slot_utilization                                                Issue Slot Utilization      76.65%      76.69%      76.67%
         21                             sm_efficiency                                               Multiprocessor Activity      97.79%      97.83%      97.80%
         21                        achieved_occupancy                                                    Achieved Occupancy    0.443988    0.444271    0.444151
         21                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    3.066346    3.069712    3.068537
         21                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
         21                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
         21                           tex_utilization                                             Unified Cache Utilization    High (7)    High (7)    High (7)
         21                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
         21                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
         21                        tex_fu_utilization                                     Texture Function Unit Utilization    High (8)    High (8)    High (8)
         21                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
         21             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
         21           single_precision_fu_utilization                            Single-Precision Function Unit Utilization    Max (10)    Max (10)    Max (10)
         21           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
         21                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
         21                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)      19.10%      19.13%      19.12%
         21                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.06%       0.06%       0.06%
         21                   sysmem_read_utilization                                        System Memory Read Utilization     Low (1)     Low (1)     Low (1)
         21                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
         21               pcie_total_data_transmitted                                           PCIe Total Data Transmitted     1583104    13265920     2337206
         21                  pcie_total_data_received                                              PCIe Total Data Received     6137856     7390208     6865261
         21                inst_executed_global_loads                              Warp level instructions for global loads   829634400   829634400   829634400
         21                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
         21                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
         21               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
         21               inst_executed_global_stores                             Warp level instructions for global stores      388800      388800      388800
         21                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
         21               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
         21              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
         21              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
         21           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
         21             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
         21          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
         21              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
         21                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
         21                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads    83318016    83405760    83352332
         21                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
         21                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
         21                           dram_read_bytes                                Total bytes read from DRAM to L2 cache    25015584    39068672    25973113
         21                          dram_write_bytes                             Total bytes written from L2 cache to DRAM    66554464    69351936    69020402
         21               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.   199065600   199065600   199065600
         21                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
         21              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
         21                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
         21                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
         21             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
         21                      global_load_requests              Total number of global load requests from Multiprocessor  3318537600  3318537600  3318537600
         21                       local_load_requests               Total number of local load requests from Multiprocessor           0           0           0
         21                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
         21                     global_store_requests             Total number of global store requests from Multiprocessor     1555200     1555200     1555200
         21                      local_store_requests              Total number of local store requests from Multiprocessor           0           0           0
         21                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
         21                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
         21                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
         21                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
         21                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
         21                         sysmem_read_bytes                                              System Memory Read Bytes       23808       29216       28534
         21                        sysmem_write_bytes                                             System Memory Write Bytes         160        1120         224
         21                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      85.30%      85.34%      85.31%
         21                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
         21                     unique_warps_launched                                              Number of warps launched       64800       64800       64800

