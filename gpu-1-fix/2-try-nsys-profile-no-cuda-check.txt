Exporting 2190 events:

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************

Exported successfully to
/home/wangpeng/tools/gitlab-william/multi-gpu-evert-cuda/gpu-1-fix/report1.sqlite

Generating CUDA API Statistics...
CUDA API Statistics (nanoseconds)




CUDA trace data was not collected.


Generating Operating System Runtime API Statistics...
Operating System Runtime API Statistics (nanoseconds)

Time(%)      Total Time       Calls         Average         Minimum         Maximum  Name                                                                            
-------  --------------  ----------  --------------  --------------  --------------  --------------------------------------------------------------------------------
   99.2     29889238683         327      91404399.6            1574       100183921  poll                                                                            
    0.7       224348749         763        294035.1            1007        91016678  ioctl                                                                           
    0.0         7507942          96         78207.7            2095         1876251  mmap                                                                            
    0.0         2244101          11        204009.2            4477          354220  open                                                                            
    0.0         1652106          30         55070.2            2006         1501679  fopen                                                                           
    0.0         1252045          59         21221.1            6640           37594  open64                                                                          
    0.0          965416          18         53634.2           12565          130410  sem_timedwait                                                                   
    0.0          449345          31         14495.0            1006          273487  munmap                                                                          
    0.0          250642           3         83547.3           79340           91848  fgets                                                                           
    0.0          216328           7         30904.0            3667          164313  fread                                                                           
    0.0          134028           1        134028.0          134028          134028  pthread_join                                                                    
    0.0           95640           2         47820.0           46902           48738  pthread_create                                                                  
    0.0           87468          19          4603.6            1100            7980  write                                                                           
    0.0           69600          25          2784.0            1020           10931  fflush                                                                          
    0.0           63121          22          2869.1            1593            4187  read                                                                            
    0.0           49324          24          2055.2            1180            4747  fclose                                                                          
    0.0           20220           7          2888.6            2634            3824  ftruncate                                                                       
    0.0           14084           8          1760.5            1015            4674  fcntl                                                                           
    0.0           13527           2          6763.5            5904            7623  socket                                                                          
    0.0           10026           1         10026.0           10026           10026  connect                                                                         
    0.0            8762           1          8762.0            8762            8762  pipe2                                                                           
    0.0            6850           2          3425.0            1305            5545  fwrite                                                                          
    0.0            3173           1          3173.0            3173            3173  bind                                                                            
    0.0            2534           1          2534.0            2534            2534  pthread_cond_broadcast                                                          
    0.0            1169           1          1169.0            1169            1169  listen                                                                          




Generating NVTX Push-Pop Range Statistics...
NVTX Push-Pop Range Statistics (nanoseconds)

