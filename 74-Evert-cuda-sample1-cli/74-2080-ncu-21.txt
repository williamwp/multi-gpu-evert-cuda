f
triangle:3061  v0: 0.003 -0.005  1.000  e01: 0.059 -0.109 -0.009  e02: 0.000  0.000  0.000  f:ff666e b:2288ff
triangle:3062  v0: 0.062 -0.115  0.991  e01:-0.059  0.110  0.009  e02:-0.011 -0.005  0.000  f:ff6670 b:2588ff
triangle:3063  v0: 0.002 -0.006  1.000  e01: 0.048 -0.115 -0.009  e02: 0.001  0.000  0.000  f:ff6670 b:2588ff
triangle:3064  v0: 0.050 -0.121  0.991  e01:-0.048  0.115  0.009  e02:-0.011 -0.004  0.000  f:ff6672 b:2988ff
triangle:3065  v0: 0.002 -0.006  1.000  e01: 0.037 -0.119 -0.009  e02: 0.001  0.000 -0.000  f:ff6672 b:2988ff
triangle:3066  v0: 0.038 -0.125  0.991  e01:-0.036  0.119  0.009  e02:-0.012 -0.003 -0.000  f:ff6674 b:2c88ff
triangle:3067  v0: 0.001 -0.006  1.000  e01: 0.025 -0.122 -0.009  e02: 0.001  0.000  0.000  f:ff6674 b:2c88ff
triangle:3068  v0: 0.025 -0.128  0.991  e01:-0.024  0.122  0.009  e02:-0.012 -0.002  0.000  f:ff6675 b:3088ff
triangle:3069  v0: 0.001 -0.006  1.000  e01: 0.013 -0.124 -0.009  e02: 0.001  0.000  0.000  f:ff6675 b:3088ff
triangle:3070  v0: 0.013 -0.130  0.991  e01:-0.012  0.124  0.009  e02:-0.012 -0.001 -0.000  f:ff6677 b:3488ff
triangle:3071  v0: 0.000 -0.006  1.000  e01: 0.001 -0.124 -0.009  e02: 0.001  0.000 -0.000  f:ff6677 b:3488ff
==PROF== Disconnected from process 3873
[3873] evert-cli@127.0.0.1
  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t *), 2023-Mar-14 23:35:09, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.33
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.36
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                              0
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.39
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          97.31
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.35
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          38.38
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t *), 2023-Mar-14 23:35:09, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.17
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.33
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.07
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.37
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          95.77
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.33
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          38.73
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t *), 2023-Mar-14 23:35:09, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.19
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.36
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                           0.07
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.54
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          96.34
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/nsecond                           1.35
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          36.70
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a

