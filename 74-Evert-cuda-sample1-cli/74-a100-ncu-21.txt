[101945] evert-cli@127.0.0.1
  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t*), 2023-Mar-15 00:29:43, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.15
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         764.41
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                              0
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.40
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          93.56
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps               %                           0.57
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         734.42
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          20.53
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t*), 2023-Mar-15 00:29:43, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.08
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         764.55
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                              0
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.29
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          93.90
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps               %                           0.67
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         734.64
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          18.15
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

  ker_mesh_shader(float, unsigned int, unsigned int, triangle_t*), 2023-Mar-15 00:29:43, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.13
    fe__draw_count.avg.pct_of_peak_sustained_elapsed                                                              (!) n/a
    gpc__cycles_elapsed.avg.per_second                                       cycle/usecond                         763.66
    gr__cycles_active.sum.pct_of_peak_sustained_elapsed                                  %                            100
    gr__dispatch_count.avg.pct_of_peak_sustained_elapsed                                                          (!) n/a
    gr__dispatch_cycles_active_queue_async.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    gr__dispatch_cycles_active_queue_sync.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    pcie__read_bytes.avg.pct_of_peak_sustained_elapsed                                   %                              0
    pcie__rx_requests_aperture_bar1_op_read.sum                                                                   (!) n/a
    pcie__rx_requests_aperture_bar1_op_write.sum                                                                  (!) n/a
    pcie__write_bytes.avg.pct_of_peak_sustained_elapsed                                  %                           0.46
    sm__cycles_active.avg.pct_of_peak_sustained_elapsed                                  %                          92.95
    sm__inst_executed_realtime.avg.pct_of_peak_sustained_elapsed                                                  (!) n/a
    sm__pipe_shared_cycles_active_realtime.avg.pct_of_peak_sustained_elaps               %                           0.93
    ed                                                                                                                   
    sm__pipe_tensor_cycles_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    sys__cycles_elapsed.avg.per_second                                       cycle/usecond                         733.74
    tpc__warps_active_shader_cs_realtime.avg.pct_of_peak_sustained_elapsed               %                          18.73
    tpc__warps_active_shader_ps_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    tpc__warps_active_shader_vtg_realtime.avg.pct_of_peak_sustained_elapse                                        (!) n/a
    d                                                                                                                    
    tpc__warps_inactive_sm_active_realtime.avg.pct_of_peak_sustained_elaps                                        (!) n/a
    ed                                                                                                                   
    tpc__warps_inactive_sm_idle_realtime.avg.pct_of_peak_sustained_elapsed                                        (!) n/a
    ---------------------------------------------------------------------- --------------- ------------------------------

